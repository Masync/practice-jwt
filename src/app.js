// Modules to npm
const express = require('express');
const app = express();
const routes = require('./Routes/index.routes');

// Accepted json and url-response
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(routes)


module.exports = app;