// Modules to npm
const app = require('./app');
require('dotenv').config({path: 'src/.env'}); 
require('./db');

// main function
async function main(){
    await app.set('port', process.env.PORT);
    app.listen(app.get('port'));
}

main();

