const _user = require('../Models/User.model');
const jwt = require('jsonwebtoken');
require('dotenv').config({path: 'src/.env'});

const signUp = async (req, res) => {
   const {nombre,correo,pass} = req.body;
   const user = new _user({
    user_name: nombre,
    email: correo,
    password: pass
   })
   user.password = await user.encryptPass(user.password);
   console.log(user);
   await user.save();
   const token = jwt.sign({id: user._id},process.env.SECRET,{
       expiresIn: 60*60*24
   })

   res.status(200).json({user,token})
   
}

const signIn = async(req, res) => {
    const {correo,pass} = req.body
    const user = await _user.findOne({email:correo})
    if (!user) {
       return res.status(404)-json({
            mssg: 'user not found'
        })
    }
    const validatep = await user.validatepass(pass)
    console.log(validatep);
    if (validatep!==true) {
       return res.status(401).json({
            mssg: 'password incorrect'
        })
    }
    const token = jwt.sign({id: user._id},process.env.SECRET)

     res.status(200).json({
        mssg: 'password correct',
        token
    })

}

const me = async(req, res) => {
   
    const user = await _user.findById(req.userId,{password:0,_id:0,__v:0})
    if (!user) {
        return res.status(404).json({
             auth:false,
             authorized: 'no user'
         })
     }
    res.status(200).json({user})
}


module.exports = {
    signUp,
    signIn,
    me
}