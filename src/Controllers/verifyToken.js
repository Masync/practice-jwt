require('dotenv').config({path: 'src/.env'}); 
const jwt = require('jsonwebtoken');

function verifyToken(req,res,next) {
    const token = req.headers['x-access-token'];
    if (!token) {
       return res.status(401).json({
            auth:false,
            authorized: 'no authorized'
        })
    }
    const decoded= jwt.verify(token, process.env.SECRET)
   req.userId = decoded.id;
   next();
}
module.exports = verifyToken;