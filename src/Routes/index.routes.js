const {Router} = require('express');
const router = Router();
const {signIn,signUp,me} = require('../Controllers/index.controller')
const verifyToken = require('../Controllers/verifyToken')

router.post('/signUp',signUp);
router.post('/signIn',signIn);
router.get('/me',verifyToken,me);

module.exports = router;