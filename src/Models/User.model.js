const {Schema,model} = require('mongoose');
const bcrypt = require('bcryptjs');
const user_schema = new Schema({
    user_name: {type: String, lowercase: true},
    email: {type: String, lowercase: true},
    password: {type: String}
});

user_schema.methods.encryptPass = async(password)=>{
    const salt = await bcrypt.genSalt(10);
    return hash  = bcrypt.hash(password,salt);
}
user_schema.methods.validatepass = function(password){
    return bcrypt.compare(password,this.password)
}
module.exports = model('User', user_schema);